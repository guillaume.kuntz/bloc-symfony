<?php

namespace App\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;


class BlogController extends Controller
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo)
    {
        $articles = $repo->findAll();

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles'=> $articles
        ]);
    }

    /**
    * @route("/" , name="home")
    */
    public function home()
    {
        return $this->render('blog/home.html.twig',[
            'title' => "Bienvenue",
            'age' => 31
        ]);
    }

    /**
    * @route("/blog/new" , name="blog_create")
    * @route("/blog/{id}/edit" , name="blog_edit")
    */
    public function form(Article $article = null, Request $request, ObjectManager $manager)
    {
        if(!$article)
        {
            $article = new Article();
        }

// creation du form relier a l'entity Article
        // $form = $this-> createFormBuilder($article)
        //              ->add('title')
        //              ->add('content')
        //              ->add('image')
        //              ->getForm();

// creation du form avec la methose php bin/console make:form
        $form = $this->createForm(ArticleType::class , $article);

// annalise de la request
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if(!$article->getId())
            {
                $article->setCreatedAt(new \DateTime());
            }

// Si valition de la request envoie
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog_show' , [
                'id' => $article->getId()
            ]);
        }

        return $this->render('blog/create.html.twig', [
            'formArticle' => $form->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
    * @route("/blog/{id}" , name="blog_show")
    */
    public function show(Article $article, Request $request, ObjectManager $manager)
    {
        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $comment->setCreatedAt(new \DateTime())
                    ->setArticle($article);

            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        return $this->render('blog/show.html.twig', [
            'article' => $article,
            'commentForm' => $form->createView()
        ]);
    }

}
