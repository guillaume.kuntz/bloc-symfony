<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bumdle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;
use App\Form\RegistrationType;

class SecurityController extends AbstractController
{
        /**
        * @route("/inscription" , name="security_registration")
        */
        public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
        {
            $user= new User();

            $form = $this->createForm(registrationType::class , $user);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid())
            {
                $hash = $encoder->encodePassword($user, $user->getPassword());

                $user->setPassword($hash);

                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute('security_login');
            }

            return $this->render('security/registration.html.twig',[
                'form' => $form->createView()
            ]);
    }

    /**
    * @route("/connexion" , name="security_login")
    */
    public function login()
    {
        return $this->render('security/login.html.twig');
    }

    /**
    * @Route("/deconnexion", name="security_logout")
    */
    public function logout(){}
}
